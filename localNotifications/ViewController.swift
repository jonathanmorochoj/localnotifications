//
//  ViewController.swift
//  localNotifications
//
//  Created by jonathan on 7/3/17.
//  Copyright © 2017 jonathan. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController, UNUserNotificationCenterDelegate {
    
    var isGrantedNotificationAccess:Bool = false
    
    @IBAction func send10SecNotification(_ sender: Any) {
    
        if isGrantedNotificationAccess{
            //add notification code here
            createNotification(timeOfInterval: 10.0)
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UNUserNotificationCenter.current().requestAuthorization(
            options: [.alert,.sound,.badge],
            completionHandler: { (granted,error) in
                self.isGrantedNotificationAccess = granted
        }
        )
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert,.sound])

    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
                
        // Determine the user action
        switch response.actionIdentifier {
        case UNNotificationDismissActionIdentifier:
            print("Dismiss Action")
        case UNNotificationDefaultActionIdentifier:
            print("Default")
        case "Snooze":
            print("Snooze")
            createNotification(timeOfInterval: 20.0)
            
            
            /*let newContent = request.content.mutableCopy() as! UNMutableNotificationContent
            newContent.body = "Snooze 20 Seconds"
            newContent.subtitle = "Snooze 20 Seconds"
            let newTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 20.0, repeats: false)
            self.addNotification(content: newContent, trigger: newTrigger, indentifier: request.identifier)
            */
        case "Delete":
            print("Delete")
        default:
            print("Unknown action")
        }
        //UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        
        completionHandler()

    }
    
    func addNotification(content:UNNotificationContent,trigger:UNNotificationTrigger?, indentifier:String){
        let request = UNNotificationRequest(identifier: indentifier, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: {
            (errorObject) in
            if let error = errorObject{
                print("Error \(error.localizedDescription) in notification \(indentifier)")
            }
        })
    }
    
    func createNotification(timeOfInterval: TimeInterval){
        
        //Set the content of the notification
        let content = UNMutableNotificationContent()
        content.title = "\(timeOfInterval) Second Notification Demo"
        content.subtitle = "From MakeAppPie.com"
        content.body = "Notification after \(timeOfInterval) seconds - Your pizza is Ready!!"
        content.categoryIdentifier = "UYLReminderCategory"
        
        //Set the trigger of the notification -- here a timer.
        let trigger = UNTimeIntervalNotificationTrigger(
            timeInterval: timeOfInterval,
            repeats: false)
        
        //Set the request for the notification from the above
        let request = UNNotificationRequest(
            identifier: "10.second.message",
            content: content,
            trigger: trigger
        )
        
        let snoozeAction = UNNotificationAction(identifier: "Snooze",
                                                title: "Snooze 20 seconds", options: [.foreground])
        //let deleteAction = UNNotificationAction(identifier: "UYLDeleteAction",title: "Delete", options: [.destructive])
        let category = UNNotificationCategory(identifier: "UYLReminderCategory",
                                              actions: [snoozeAction],
                                              intentIdentifiers: [], options: [])
        UNUserNotificationCenter.current().setNotificationCategories([category])
        //UNMutableNotificationContent().categoryIdentifier = "UYLReminderCategory"
        
        //Add the notification to the currnet notification center
        UNUserNotificationCenter.current().delegate = self
        //UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().add(
            request, withCompletionHandler: nil)

    }
    
}
